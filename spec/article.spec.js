var React = require('react');
var TestUtils = require('react/lib/ReactTestUtils'); //I like using the Test Utils, but you can just use the DOM API instead.
var expect = require('expect');

import Nav from '../src/js/components/layout/Nav';
import Footer from '../src/js/components/layout/Footer';
import Article from '../src/js/components/Article';
import Archives from '../src/js/pages/Archives';


describe('test case one', function () {
  it('renders without problems', function (done) {
    var root = TestUtils.renderIntoDocument(<Article/>);
    expect(root).toExist();
    console.log('--------------------------------------');
    //expect(1).toBe(2);
    done();
  });
  it('test success', () => {
    expect(1).toBe(1);

  });
});
