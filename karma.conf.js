var webpack = require('webpack');
var path = require('path');

module.exports = function (config) {
  config.set({
    basePath: '.',
    browsers: [ 'Chrome' ], //run in Chrome
    singleRun: true, //just run once by default
    frameworks: [ 'mocha' ], //use the mocha test framework
    //logLevel: config.LOG_DEBUG,
    plugins: [
      'karma-chrome-launcher',
      'karma-phantomjs-launcher',
      'karma-mocha',
      'karma-sourcemap-loader',
      'karma-webpack',
      'karma-coverage',
      'karma-mocha-reporter'
    ],
    files: [
      'spec/index.js' //just load this file
    ],
    preprocessors: {
      'spec/index.js': [ 'webpack', 'sourcemap'], //preprocess with webpack and our sourcemap loader
    },
    webpack: { //kind of a copy of your webpack config
      devtool: 'inline-source-map', //just do inline source maps instead of the default
      module: {
        loaders: [
          {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /(node_modules|bower_components)/,
            query: {
              presets: ['react', 'es2015', 'stage-0'],
              plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
            }
          }
        ],
        postLoaders: [ { //delays coverage til after tests are run, fixing transpiled source coverage error
          test: /\.js$/,
          include: path.resolve('src/'),
          loader: 'istanbul-instrumenter' } ]
      }
    },
    webpackServer: {
      noInfo: true //please don't spam the console when running in karma!
    },
    reporters: [ 'mocha', 'coverage' ], //report results in this format
    coverageReporter: {
      //type: 'html', //produces a html document after code is run
      //dir: 'coverage/' //path to created html doc
      reporters: [
        {
          type: 'html',
          dir: 'coverage/'
        },
        {
          type: 'text'
        }
      ]
    },
    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    }
  });
};