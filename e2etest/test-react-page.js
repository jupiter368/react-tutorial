module.exports = {
  'Demo test Google' : function (browser) {
    browser
      .url('http://localhost:8080')
      .waitForElementVisible('body', 1000)
      .assert.containsText('.well.text-center', 'Ad spot')

      .waitForElementVisible('a[href="#archives"]', 1000)
      .click('a[href="#archives"]')
      .assert.visible('h1')

      .waitForElementVisible('a[href="#settings"]', 1000)
      .click('a[href="#settings"]')
      .assert.visible('h1')

      .pause(1000)
      .end();
  },

  'EM7 Dashboard': function(browser) {
    browser
      .url('http://192.168.38.237/')
      .waitForElementVisible('body', 1000)

      .setValue('#LOGIN_user', 'em7admin')
      .setValue('#LOGIN_pwd', 'em7admin')
      .click('#submit')

      .waitForElementVisible('.tabbar', 3000)
      .click('a[href*="dashboards"]')

      .waitForElementVisible('.dashboard_widgets', 20000)
      .click('#create_dashboard')

      .waitForElementVisible('.dashboard_grid', 20000)
      .click('#actions')

      .waitForElementVisible('#actions_flyout', 1000)
      .click('#new_widget')
      .pause(3000) //wait for iframe to show up

      .frame('TB_iframeContent', function(event) { //change to inner frame
        console.log(event.status);
      })
      .waitForElementVisible('#body_container', 10000)
      .waitForElementVisible('#dtype_3', 1000)
      .click('#dtype_3')
      .pause(3000)

      .click('#save')

      .frameParent() //change back to parent frame
      .pause(3000)
      .click('#actions')

      .waitForElementVisible('#actions_flyout', 1000)
      .click('#delete_dashboard')
      .pause(1000)
      .acceptAlert()

      .pause(1000)
      .end();
  }
};