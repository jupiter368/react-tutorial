http://nightwatchjs.org/
https://webpack.github.io/
https://mochajs.org/
https://facebook.github.io/react/docs/test-utils.html
https://karma-runner.github.io/0.13/index.html

setup:
    npm install (in project folder)

commands:
    npm run dev (to run webpack live loader on)
        inline mode: http://localhost:8080/
        iframe mode: http://localhost:8080/webpack-dev-server/index.html

    npm run test (to run mocha test and coverage report)

    npm run e2etest (run end to end test)

    npm run build (build production bundle)

    npm run analyse (generates stats.json upload to https://webpack.github.io/analyse/#home)