import React from "react";
const mName = '../components/async_module';


export default class Settings extends React.Component {
  _handleClick() {
    require.ensure([], function() {
      var async = require('../components/async_module');
      console.log(async);
    }, 'async_module');
  }
  render() {
    return (
      <div>
        <h1>Settings</h1>
        <button onClick={this._handleClick}>load async</button>
      </div>
    );
  }
}
